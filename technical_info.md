# Technical Info

## The static scanner problem
When working in Kotlin we want to be able to define a scanner outside the main (static), so that can be rehused by
functions.

### Problem 1
Output is redirected per test, Input, is now redirected once. In this way, the static scanner can be rehused

### Problem 2
Is a test execution finishes with a nextInt(), and the next one starts with nextLine(), the scanner has some kind of
cache, and the last not read line is used in the second test.

Hyperskyll tests framework fixes the issue, but have not found how.

The solution is based on this awnser on Stack Overflow:
https://stackoverflow.com/questions/42102/using-different-classloaders -for-different-junit-tests#answer-34154189

We try to recreate the class each time with the static variables. This is done by SeparateClassLoader

However to use this solution we can't use java 11, so we have changed docker to java 8.