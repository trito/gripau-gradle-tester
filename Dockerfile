FROM gradle:6.9.2-jdk11

COPY --chown=gradle:gradle . .
RUN rm -rf /home/gradle/build/test-results/*
RUN rm -rf /home/gradle/build/reports/*

CMD gradle reportedAssemble detektMain checkstyleMain test --no-daemon
