package com.example.trito.gripautest

import java.util.*

/**
 * If input is 🌳 returns _🌳_ else _Not read correctly_
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val word = scanner.next()
    if(word=="\uD83C\uDF33") println("\uD83C\uDF33")
    else println("Not read correctly")
}