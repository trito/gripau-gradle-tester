package com.example.trito.gripautest

import kotlinx.serialization.Serializable

@Serializable
data class Rectangle(val height: Double, val width: Double)