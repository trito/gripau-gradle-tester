package com.example.trito.gripautest;

import java.util.Scanner;

public class InputParseErrorSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextInt();
    }
}
