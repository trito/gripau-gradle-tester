package dev.trito.gripau;

import dev.trito.gripau.DynamicTestFromFileTestBase;

public class DynamicTestFromFileLocalTest extends DynamicTestFromFileTestBase {
    public DynamicTestFromFileLocalTest() {
        setTestCasesFolder("src/test/testdata/testcases");
        setUserFile("src/test/testdata/user.yml");
    }
}
