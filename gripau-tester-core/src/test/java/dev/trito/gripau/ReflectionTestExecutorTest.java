package dev.trito.gripau;


import dev.trito.gripau.exceptions.GripauInputParsingException;
import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.io.OutputHandler;
import org.junit.jupiter.api.*;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.*;

public class ReflectionTestExecutorTest {
    OutputHandler outputHandler;
    ReflectionTestExecutor executor;

    String className = "com.example.trito.gripautest.SomeClass";
    String param = "param1";
    String[] stringParams = new String[]{"\""+param+"\""};

    @BeforeAll
    public static void redirectInput(){
        InputHandler.get().initialize();
    }

    @AfterAll
    public static void restoreInput(){
        InputHandler.get().restore();
    }

    @BeforeEach
    public void prepare(){
        outputHandler = new OutputHandler();
        outputHandler.initialize();
        executor = new ReflectionTestExecutor(outputHandler);
    }

    @AfterEach
    public void restore(){
        outputHandler.restore();
    }


    @Test
    public void invokeFunctionForClassTest() {
        Object result = executor.invokeFunctionForClass(className, "someMethod", stringParams);

        assertEquals(param, result);
    }

    @Test
    public void invokeFunctionForClassTestVoidFunction() {
        String param = "param1";
        Object result = executor.invokeFunctionForClass(className, "voidTwoParams", new String[]{"\""+param+"\"", "5"});

        assertNull(result);
    }

    @Test
    public void executeTestCase() {
        String param = "param1";
        executor.executeTestCase(className, "someMethod", stringParams, "\""+param+"\"", "", "");
    }

    @Test
    public void executeTestCaseFailsWithInvalidResult() {
        String param = "param1";
        AssertionFailedError exception = assertThrows(AssertionFailedError.class, () -> {
            executor.executeTestCase(className, "someMethod", stringParams, "\"ANOTHER\"", "", "");
        });
    }

    @Test
    public void executeTInputParseErrorShodReturnException() {
        String className = "com.example.trito.gripautest.InputParseErrorSample";
        AssertionFailedError exception = assertThrows(AssertionFailedError.class, () -> {
            executor.executeTestCase(className, "\"TEXT\"", "");
        });
        assertEquals(GripauInputParsingException.class, exception.getCause().getClass());
    }

}
