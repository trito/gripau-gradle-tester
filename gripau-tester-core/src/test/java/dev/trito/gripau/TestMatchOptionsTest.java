package dev.trito.gripau;

import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.models.*;
import org.junit.jupiter.api.*;
import org.opentest4j.AssertionFailedError;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestMatchOptionsTest {
    DynamicTestFromFileTestBase dynamicTestFromFileTestBase;
    User user = new User("com.example.trito", "kotlin");

    @BeforeAll
    public static void redirectInput(){
        InputHandler.get().initialize();
    }

    @AfterAll
    public static void restoreInput(){
        InputHandler.get().restore();
    }

    @BeforeEach
    public void prepare(){
        dynamicTestFromFileTestBase = new DynamicTestFromFileTestBase();
    }

    private TestSuite createTestSuite(List<ExerciceData> exerciceDatas){
        return new TestSuite("gripautest", "test", exerciceDatas);
    }

    private void testPrintsOutputOn(String input, String output, TestMatchOptions testMatchOptions) throws Throwable {
        TestData testData = new TestData("t1", input, output, null, null);
        List<ExerciceData> exerciceDatas = Arrays.asList(new ExerciceData("SomeKotlinFile", Arrays.asList(testData), testMatchOptions));
        DynamicTest test = dynamicTestFromFileTestBase.createDynamicTest(user, createTestSuite(exerciceDatas), exerciceDatas.get(0), testData);
        test.getExecutable().execute();
    }

    @Test
    public void defaultValues() throws Throwable {
        testPrintsOutputOn("XXXaa     aaXXX", "AaA  \na", null);
    }

    @Test
    public void spaceChangeSensitiveTest()  {
        TestMatchOptions testMatchOptions = new TestMatchOptions(false, false, true);
        assertThrows(AssertionFailedError.class, () ->
                testPrintsOutputOn("aaaa", "aa aa", testMatchOptions)
        );
    }

    @Test
    public void caseSesitiveTest() throws Throwable {
        TestMatchOptions testMatchOptions = new TestMatchOptions(true, false, false);
        assertThrows(AssertionFailedError.class, () ->
                testPrintsOutputOn("aaaa", "AaAa", testMatchOptions)
        );
    }

    @Test
    public void strictMatchSensitiveTest() throws Throwable {
        TestMatchOptions testMatchOptions = new TestMatchOptions(false, true, false);
        assertThrows(AssertionFailedError.class, () ->
                testPrintsOutputOn("aaaa", "XaaaaX", testMatchOptions)
        );
    }
}
