package dev.trito.gripau;


import com.example.trito.gripautest.KotlinFileWithStaticScannerKt;
import dev.trito.gripau.exceptions.GripauInputParsingException;
import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.io.OutputHandler;
import org.junit.jupiter.api.*;
import org.opentest4j.AssertionFailedError;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertThrows;


public class InputCasesTest {
    public static class ReadsTwoLinesMain{
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
            scanner.nextLine();
        }
    }

    OutputHandler outputHandler;
    ReflectionTestExecutor executor;

    String className = KotlinFileWithStaticScannerKt.class.getName();

    @BeforeAll
    public static void redirectInput(){
        InputHandler.get().initialize();
    }

    @AfterAll
    public static void restoreInput(){
        InputHandler.get().restore();
    }

    @BeforeEach
    public void prepare(){
        outputHandler = new OutputHandler();
        outputHandler.initialize();
        executor = new ReflectionTestExecutor(outputHandler);
    }

    @AfterEach
    public void restore(){
        outputHandler.restore();
    }

    @Test
    public void invokeMainWithShortInput() {
        assertThrows(AssertionFailedError.class, () ->
            executor.executeTestCase(ReadsTwoLinesMain.class.getName(), "", ""));
    }



}
