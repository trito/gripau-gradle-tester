package dev.trito.gripau;


import com.example.trito.gripautest.KotlinFileWithStaticScannerKt;
import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.io.OutputHandler;
import org.junit.jupiter.api.*;

public class KotlinFileWithStaticScannerTest {
    OutputHandler outputHandler;
    ReflectionTestExecutor executor;

    String className = KotlinFileWithStaticScannerKt.class.getName();

    @BeforeAll
    public static void redirectInput(){
        InputHandler.get().initialize();
    }

    @AfterAll
    public static void restoreInput(){
        InputHandler.get().restore();
    }

    @BeforeEach
    public void prepare(){
        outputHandler = new OutputHandler();
        outputHandler.initialize();
        executor = new ReflectionTestExecutor(outputHandler);
    }

    @AfterEach
    public void restore(){
        outputHandler.restore();
    }

    @Test
    public void invokeMainForClassTest() {
        executor.executeTestCase(className, "hello", "hello");
    }

    @Test
    public void invokeMainForClassTest2() {
        executor.executeTestCase(className, "goodbye", "goodbye");
    }



}
