package dev.trito.gripau;

public class KotlinDynamicTestFromFileLocalTest extends DynamicTestFromFileTestBase {
    public KotlinDynamicTestFromFileLocalTest() {
        setTestCasesFolder("src/test/kotlintestdata/testcases");
        setUserFile("src/test/kotlintestdata/user.yml");
    }
}
