package dev.trito.gripau;


import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.io.OutputHandler;
import org.junit.jupiter.api.*;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.*;

public class KotlinReflectionTestExecutorTest {
    OutputHandler outputHandler;
    ReflectionTestExecutor executor;

    String className = "com.example.trito.gripautest.SomeKotlinFileKt";
    String param = "param1";
    String function = "printsAndReturnsParam";
    String[] stringParams = new String[]{"\""+param+"\""};

    @BeforeAll
    public static void redirectInput(){
        InputHandler.get().initialize();
    }

    @AfterAll
    public static void restoreInput(){
        InputHandler.get().restore();
    }

    @BeforeEach
    public void prepare(){
        outputHandler = new OutputHandler();
        outputHandler.initialize();
        executor = new ReflectionTestExecutor(outputHandler);
    }

    @AfterEach
    public void restore(){
        outputHandler.restore();
    }

    @Test
    public void invokeMainForClassTest() {
        String desiredOutput = "SampleInputOutput";
        executor.executeTestCase(className, desiredOutput, desiredOutput);
    }

    @Test
    public void invokeMainForClassTestSkipsSpaces() {
        executor.executeTestCase(className, "SampleInputOutput", "S   a mp le\n     In  p\nutOutput  ");
    }


    @Test
    public void invokeFunctionForClassTest() {
        Object result = executor.invokeFunctionForClass(className, function, stringParams);

        assertEquals(param, result);
    }
    //
//    @Test
//    public void invokeFunctionForClassTestVoidFunction() {
//        String param = "param1";
//        Object result = executor.invokeFunctionForClass(className, "voidTwoParams", new String[]{"\""+param+"\"", "5"});
//
//        assertNull(result);
//    }
//


    @Test
    public void executeTestCaseOnprintsFirstReturnsVoid() {
        String[] stringParams = new String[]{"\""+param+"\"", "\""+param+"\""};
        executor.executeTestCase(className, "printsFirstReturnsVoid", stringParams, null, "", "");
    }

    @Test
    public void executeTestCase() {
        executor.executeTestCase(className, function, stringParams, "\""+param+"\"", "", param);
    }

    @Test
    public void executeTestCaseWithInvalidOutput() {
        assertThrows(AssertionFailedError.class, () ->
            executor.executeTestCase(className, function, stringParams, "\""+param+"\"", "", "INCORRECT_OUTPUT"));
    }

    @Test
    public void executeTestCaseFailsWithInvalidResult() {
        assertThrows(AssertionFailedError.class, () ->
            executor.executeTestCase(className, function, stringParams, "\"ANOTHER\"", "", ""));
    }

    @Test
    public void executereturnDataClassThatReceives() {
        String objectYml = "{'text': 'Hello', 'number': 45}";
        String[] stringParams = new String[]{objectYml};
        executor.executeTestCase(className, "returnDataClassThatReceives", stringParams, objectYml, "", "");
    }

    @Test
    public void specialUtfCharsTest() {
        executor.executeTestCase("com.example.trito.gripautest.SpecialUtfCharsSampleKt", "🌳", "🌳");
    }

//
//    @Test
//    public void executeTInputParseErrorShodReturnException() {
//        String className = "com.example.trito.gripautest.InputParseErrorSample";
//        AssertionFailedError exception = assertThrows(AssertionFailedError.class, () -> {
//            executor.executeTestCase(className, "\"TEXT\"", "");
//        });
//        assertEquals(GripauInputParsingException.class, exception.getCause().getClass());
//    }

}
