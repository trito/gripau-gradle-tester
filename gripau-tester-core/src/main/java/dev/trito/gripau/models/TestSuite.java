package dev.trito.gripau.models;

import java.util.List;

public class TestSuite {
        String subPackage;
        String name;
        List<ExerciceData> exercices;

        public TestSuite(){}

    public TestSuite(String subPackage, String name, List<ExerciceData> exercices) {
        this.subPackage = subPackage;
        this.name = name;
        this.exercices = exercices;
    }

    public String getName() {
        return name;
    }

    public String getSubPackage() {
            return subPackage;
        }

        public List<ExerciceData> getExercices() {
            return exercices;
        }
    }