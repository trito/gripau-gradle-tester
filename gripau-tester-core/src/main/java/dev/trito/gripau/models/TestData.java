package dev.trito.gripau.models;

public class TestData{
    String name;
    String input;
    String output;

    String[] paramsYaml;
    String resultYaml;

    public TestData() {
    }

    public TestData(String name, String input, String output, String[] paramsYaml, String resultYaml) {
        this.name = name;
        this.input = input;
        this.output = output;
        this.paramsYaml = paramsYaml;
        this.resultYaml = resultYaml;
    }

    public String getName() {
        return name;
    }

    public String getInput() {
        return input;
    }

    public String getOutput() {
            return output;
        }

    public String getResultYaml() {
        return resultYaml;
    }

    public String[] getParamsYaml() {
        return paramsYaml;
    }
}