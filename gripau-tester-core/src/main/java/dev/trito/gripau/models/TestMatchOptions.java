package dev.trito.gripau.models;

/**
 * To be used in the future to configure text matching
 * ignoreCase: if true ignores case
 * strictMatch: if true check for exact match, if false only if it contains the text (xxaaxx matches aa).
 * spaceChangeSensitive: if false removes all spaces, tabs and break lines before comparison
 */
public class TestMatchOptions {
    boolean caseSensitive = false;
    boolean strictMatchSensitive = false;
    boolean spaceChangeSensitive = false;

    public TestMatchOptions() {
    }

    public TestMatchOptions(boolean caseSensitive, boolean strictMatchSensitive, boolean spaceChangeSensitive) {
        this.caseSensitive = caseSensitive;
        this.strictMatchSensitive = strictMatchSensitive;
        this.spaceChangeSensitive = spaceChangeSensitive;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public boolean isStrictMatchSensitive() {
        return strictMatchSensitive;
    }

    public boolean isSpaceChangeSensitive() {
        return spaceChangeSensitive;
    }
}