package dev.trito.gripau.models;

import java.util.List;

public class ExerciceData {
    String className;
    List<TestData> tests;
    String methodName;
    TestMatchOptions testMatchOptions;

    public ExerciceData() {
    }

    public ExerciceData(String className, List<TestData> tests, TestMatchOptions testMatchOptions) {
        this(className, tests, null, testMatchOptions);
    }

    public ExerciceData(String className, List<TestData> tests, String methodName, TestMatchOptions testMatchOptions) {
        this.className = className;
        this.tests = tests;
        this.methodName = methodName;
        this.testMatchOptions = testMatchOptions;
    }

    public String getClassName() {
        return className;
    }


    public String getMethodName() {
        return methodName;
    }

    public List<TestData> getTests() {
            return tests;
        }

    public TestMatchOptions getTestMatchOptions() {
        return testMatchOptions;
    }

    public TestMatchOptions getTestMatchOptionsOrDefault() {
        if(getTestMatchOptions()!=null)
            return getTestMatchOptions();
        else
            return new TestMatchOptions();
    }
}

