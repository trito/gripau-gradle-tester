package dev.trito.gripau.models;

public class User {
    public static final String LANGUAGE_JAVA = "java";
    public static final String LANGUAGE_KOTLIN = "kotlin";
    String mainPackage;
    String language;

    public User(){}

    public User(String mainPackage) {
            this.mainPackage = mainPackage;
        }

    public User(String mainPackage, String language) {
        this.mainPackage = mainPackage;
        if(language==null)
            language = LANGUAGE_JAVA;
        this.language = language;
    }

    public String getMainPackage() {
            return mainPackage;
        }

    public String getLanguage() {
        return language;
    }

    public boolean isKotlin() {
        return getLanguage()!=null && getLanguage().equals(LANGUAGE_KOTLIN);
    }
}

