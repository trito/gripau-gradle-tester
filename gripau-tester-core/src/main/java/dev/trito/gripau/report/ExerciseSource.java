package dev.trito.gripau.report;

public class ExerciseSource {
    String file;
    String source;

    public ExerciseSource(String file, String source) {
        this.file = file;
        this.source = source;
    }

    public String getFile() {
        return file;
    }

    public String getSource() {
        return source;
    }
}
