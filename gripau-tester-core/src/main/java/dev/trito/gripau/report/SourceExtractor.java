package dev.trito.gripau.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dev.trito.gripau.DynamicTestFromFileTestBase;
import dev.trito.gripau.exceptions.GripauException;
import dev.trito.gripau.models.ExerciceData;
import dev.trito.gripau.models.TestSuite;
import dev.trito.gripau.models.User;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;

public class SourceExtractor {
    public void clear() {
        try {
            deleteDirectoryStream(getReportsPath());
        } catch (IOException e) {}
    }

    void deleteDirectoryStream(Path path) throws IOException {
        Files.walk(path)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

    public static Path getReportsPath(){
        return Paths.get("build", "reports", "gripau");
    }

    public void extractKotlinSource(User user, TestSuite testSuite, ExerciceData exercice) {
        try {
            String suiteName = testSuite.getName();
            String exerciceName = exercice.getClassName();

            Path destFolderPath = getReportsPath();
            Files.createDirectories(destFolderPath);

            Path originalPath = getSourcePath(user, testSuite, exercice);
            String source = new String(Files.readAllBytes(originalPath));
            String cleanSource = source.replaceAll("(?m)^import .*", "").replaceAll("(?m)^package .*", "").trim();
            ExerciseReport exerciseReport = new ExerciseReport(exerciceName, Arrays.asList(new ExerciseSource(originalPath.toString(), cleanSource)));
            Path destFile = destFolderPath.resolve(exerciceName+".yml");

            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            mapper.writeValue(destFile.toFile(), exerciseReport);
        } catch (IOException e) {
            // no sources found
        }
    }

    private Path getSourcePath(User user, TestSuite testSuite, ExerciceData exercice) {
        String javaClassName = DynamicTestFromFileTestBase.getJavaClassName(user, testSuite, exercice);
        String path = javaClassName.replace(".", "/");
        if(user.isKotlin())
            path = path+".kt";
        Path originalPath = Paths.get("src", "main", "java", path);
        return originalPath;
    }

}
