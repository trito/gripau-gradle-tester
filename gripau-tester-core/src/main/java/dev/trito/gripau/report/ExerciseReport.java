package dev.trito.gripau.report;

import java.util.List;

public class ExerciseReport {
    String name;
    List<ExerciseSource> sources;

    public ExerciseReport(String name, List<ExerciseSource> sources) {
        this.name = name;
        this.sources = sources;
    }

    public String getName() {
        return name;
    }

    public List<ExerciseSource> getSources() {
        return sources;
    }
}
