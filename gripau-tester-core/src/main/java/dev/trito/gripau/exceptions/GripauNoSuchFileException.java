package dev.trito.gripau.exceptions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class GripauNoSuchFileException extends NoSuchFileException{
    public GripauNoSuchFileException(String file) {
        super(file);
    }

    public GripauNoSuchFileException(String file, String other, String reason) {
        super(file, other, reason);
    }

    public static IOException fromNoSuchFileException(NoSuchFileException e)  {
        Path path = Paths.get(e.getFile());
        while(!Files.exists(path) && path.getParent()!=null){
            path = path.getParent();
        }
        try {
            String info = Files.list(path).collect(Collectors.toList()).toString();
            return new GripauNoSuchFileException(e.getFile(), info, e.getReason());
        } catch (IOException ioException) {
            return ioException;
        }

    }

}
