package dev.trito.gripau.exceptions;

public class GripauException extends RuntimeException{

    public GripauException(String message) {
        super(message);
    }

    public GripauException(Throwable cause) {
        super(cause);
    }

    public GripauException(String message, Throwable cause) {
        super(message, cause);
    }
}
