package dev.trito.gripau.exceptions;

import dev.trito.gripau.exceptions.GripauException;

public class GripauInputParsingException extends GripauException {
    public GripauInputParsingException(String message) {
        super(message);
    }

    public GripauInputParsingException(Throwable cause) {
        super(cause);
    }
}
