package dev.trito.gripau.exceptions;

public class ExerciceClassNotFoundException extends GripauException {
    public ExerciceClassNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
