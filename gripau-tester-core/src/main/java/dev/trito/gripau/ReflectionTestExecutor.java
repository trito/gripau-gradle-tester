package dev.trito.gripau;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dev.trito.gripau.exceptions.ExerciceClassNotFoundException;
import dev.trito.gripau.exceptions.GripauException;
import dev.trito.gripau.exceptions.GripauInputParsingException;
import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.io.OutputHandler;
import dev.trito.gripau.exceptions.GripauNoSuchFileException;
import dev.trito.gripau.models.TestMatchOptions;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.module.kotlin.ExtensionsKt.registerKotlinModule;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.junit.jupiter.api.Assertions.fail;

public class ReflectionTestExecutor {
    final static Logger logger = LoggerFactory.getLogger("GripauGradleTester");

    OutputHandler outputHandler;

    public ReflectionTestExecutor(OutputHandler outputHandler) {
        this.outputHandler = outputHandler;
    }

    public void executeTimedOutTest(long milis, String className, String input, String output){
        executeTimedOutTest(milis, className, input, output, new TestMatchOptions());
    }

    public void executeTimedOutTest(long milis, String className, String input, String output, TestMatchOptions testMatchOptions){
        assertTimeoutPreemptively(Duration.ofMillis(milis), () -> {
            executeTestCase(className, input, output, testMatchOptions);
        });
    }

    public void executeTimedOutTest(long milis, String className, String method, String[] paramsYml, String desiredResultYaml, String input, String output){
        executeTimedOutTest(milis,  className,  method, paramsYml,  desiredResultYaml,  input,  output, new TestMatchOptions());
    }

    public void executeTimedOutTest(long milis, String className, String method, String[] paramsYml, String desiredResultYaml, String input, String output, TestMatchOptions testMatchOptions){
        assertTimeoutPreemptively(Duration.ofMillis(milis), () -> {
            executeTestCase(className, method, paramsYml, desiredResultYaml, input, output, testMatchOptions);
        });
    }

    public void executeTestCase(String className, String input, String desiredOutput){
        executeTestCase(className, input, desiredOutput, new TestMatchOptions());
    }

    public void executeTestCase(String className, String input, String desiredOutput, TestMatchOptions testMatchOptions){
        logger.warn("GripauGradleTester: "+ className +" input: "+input+ " output: "+desiredOutput);
        InputHandler.get().simulateInput(input);
        invokeMainForClass(className);
        String producedOutput = outputHandler.getOutput();

        OutputValidator.assertIsValidOutput(testMatchOptions, producedOutput, desiredOutput);
    }

    public void executeTestCase(String className, String method, String[] paramsYml, String desiredResultYaml, String input, String desiredOutput){
        executeTestCase(className, method, paramsYml,  desiredResultYaml, input, desiredOutput, new TestMatchOptions());
    }

    public void executeTestCase(String className, String method, String[] paramsYml, String desiredResultYaml, String input, String desiredOutput, TestMatchOptions testMatchOptions){
        logger.warn("GripauGradleTester: "+ className +" input: "+input+ " output: "+desiredOutput);
        InputHandler.get().simulateInput(input);
        Object result = invokeFunctionForClass(className, method, paramsYml);
        String producedOutput = outputHandler.getOutput();

        if(desiredOutput!=null)
            OutputValidator.assertIsValidOutput(testMatchOptions, producedOutput, desiredOutput);
        OutputValidator.assertIsValidResult(result, desiredResultYaml);
    }

    public static Method getMethodByName(Class<?> clazz, String name){
        Method foundMethod = null;
        for(Method method: clazz.getMethods()){
            if(method.getName().equals(name)) {
                if(foundMethod!=null)
                    throw new UnsupportedOperationException("Only works with one method with given name: "+name);
                foundMethod = method;
            }
        }
        if(foundMethod!=null)
            return foundMethod;
        throw new UnsupportedOperationException("Method not found: "+name+". Existing="+ Arrays.stream(clazz.getMethods()).map(method -> method.getName()).collect(Collectors.joining()));
    }



    public static List<Object> prepareParameterValues(Method method, String[] args) throws IOException {
        ObjectMapper mapper = registerKotlinModule(new ObjectMapper(new YAMLFactory()));
        List<Object> funcParamVaules = new ArrayList<>();
        if(args!=null) {
            for (int i = 0; i < args.length; i++) {
                Parameter parameter = method.getParameters()[i];
                String arg = args[i];
                Object o = mapper.readValue(arg, parameter.getType());
                funcParamVaules.add(o);
            }
        }
        return funcParamVaules;
    }

    public Object invokeFunctionForClass(String className, String methodName, String[] args) throws GripauException {

        try {
            // Only for java 1.8, solves static variables rehused like scanner
            //Class<?> clazz = new SeparateClassLoader().loadClassWithSeparateClassLoader(className, className);
            Class<?> clazz = Class.forName(className);
            Method method = getMethodByName(clazz, methodName);
            List<Object> params = prepareParameterValues(method, args);

            ForbidExitSecurityManager secManager = new ForbidExitSecurityManager();
            System.setSecurityManager(secManager);
            try {
                return method.invoke(null, params.toArray());
            } catch (SecurityException e) {
                fail("System.exit is forbidden");
            }
        } catch (ClassNotFoundException e) {
            throw new ExerciceClassNotFoundException("No existeix la classe "+className, e);
//        } catch (NoSuchMethodException e) {
//            fail(e);
        } catch (IllegalAccessException e) {
            fail(e);
        } catch (InvocationTargetException e) {
            Throwable throwable = e.getTargetException();
            if (throwable instanceof InputMismatchException)
                Assertions.fail(new GripauInputParsingException(throwable));
            else
                fail(e.getTargetException());
        } catch (NoSuchFileException e) {
            fail(GripauNoSuchFileException.fromNoSuchFileException((NoSuchFileException)e));
        } catch (IOException e) {
            fail(e);
        }
        throw new UnsupportedOperationException();
    }

    public void invokeMainForClass(String className) throws GripauException {
        String[] args = {};
        try {
            Class<?> clazz = Class.forName(className);


            Method mainMethod = clazz.getMethod("main", String[].class);

            ForbidExitSecurityManager secManager = new ForbidExitSecurityManager();
            System.setSecurityManager(secManager);
            try {
                mainMethod.invoke(null, new Object[] {args});
            } catch (SecurityException e) {
                fail("System.exit is forbidden");
            }
        } catch (ClassNotFoundException e) {
            throw new ExerciceClassNotFoundException("No existeix la classe "+className, e);
        } catch (NoSuchMethodException e) {
            fail(e);
        } catch (IllegalAccessException e) {
            fail(e);
        } catch (InvocationTargetException e) {
            Throwable throwable = e.getTargetException();
            if(throwable instanceof InputMismatchException){
                fail(new GripauInputParsingException(throwable));
            }
            fail(e.getTargetException());
        }
    }
}
