package dev.trito.gripau;

import java.net.URLClassLoader;


public class SeparateClassLoader {

  /**
   * Create a new class loader for loading application-dependent code and return an instance of that.
   */
  @SuppressWarnings("unchecked")
  public Class<?> loadClassWithSeparateClassLoader(String packageName, String className) throws ClassNotFoundException {
    TestApplicationClassLoader cl = new TestApplicationClassLoader(packageName);
    Class<?> testerClass = cl.loadClass(className);
    //Class.forName(clazz.getName(), true, testClassLoader);
    return testerClass;
  }


  /**
   * Loads application classes in separate class loader from test classes.
   */
  private static class TestApplicationClassLoader extends URLClassLoader {
    private final String appPackage;

    public TestApplicationClassLoader(String appPackage) {
      super(((URLClassLoader) getSystemClassLoader()).getURLs());
      this.appPackage = appPackage;
    }

    @Override
    public Class<?> loadClass(String className) throws ClassNotFoundException {
      if (isApplicationClass(className)) {
        return super.findClass(className);
      }
      return super.loadClass(className);
    }

    private boolean isApplicationClass(String className) {
      return className.startsWith(appPackage);
    }
  }
}
