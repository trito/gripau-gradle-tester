package dev.trito.gripau;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dev.trito.gripau.io.InputHandler;
import dev.trito.gripau.io.OutputHandler;
import dev.trito.gripau.models.*;
import dev.trito.gripau.report.SourceExtractor;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class DynamicTestFromFileTestBase {
    public static final int TIME_OUT_MILIS = 2000;
    String mainFolder;
    OutputHandler outputHandler = new OutputHandler();
    ReflectionTestExecutor reflectionTestExecutor = new ReflectionTestExecutor(outputHandler);
    String testCasesFolder = "testdata/testcases";
    String userFile = "testdata/user.yml";

    public void setUpStreams() {
        outputHandler.initialize();
    }

    public void restoreStreams() {
        outputHandler.restore();
    }

    @BeforeAll
    public static void redirectInput(){
        new SourceExtractor().clear();
        InputHandler.get().initialize();
    }

    @AfterAll
    public static void restoreInput(){
        InputHandler.get().restore();
    }

    @TestFactory
    Stream<DynamicTest> dynamicTestsWithIterable() throws IOException {
        User user = readUser();
        Stream<TestSuite> testSuites = readTestSuites();
        Stream<DynamicTest> tests =  testSuites.flatMap(testSuite ->
                testSuite.getExercices().stream().flatMap(exercice ->
                        //return Arrays.stream(testDatas).map(testData ->
                        createDynamicTests(user, testSuite, exercice)
                ));
        return tests;
    }

    public TestSuite readTestSuite(Path path) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        return mapper.readValue(path.toFile(), TestSuite.class);
    }

    public Stream<TestSuite> readTestSuites() throws IOException {
        Stream<Path> paths = Files.walk(Paths.get(testCasesFolder));
        return paths.filter(Files::isRegularFile)
                    .map(path -> {
                        try {
                            return readTestSuite(path);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
    }

    public User readUser() throws IOException {
        String mainPackage = System.getenv("USER_MAIN_PACKAGE");
        String language = System.getenv("USER_LANGUAGE");
        if(mainPackage!=null && !mainPackage.equals("")) {
            return new User(mainPackage, language);
        }
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        return mapper.readValue(new File(userFile), User.class);
    }



    private Stream<DynamicTest> createDynamicTests(User user, TestSuite testSuite, ExerciceData exercice) {
        new SourceExtractor().extractKotlinSource(user, testSuite, exercice);
        return exercice.getTests().stream().map(test ->
                createDynamicTest(user, testSuite, exercice, test)
        );
    }

    public static String getJavaClassName(User user, TestSuite testSuite, ExerciceData exercice){
        return user.getMainPackage() + "." + testSuite.getSubPackage() +"." + exercice.getClassName();
    }

    public static String getKotlinClassName(User user, TestSuite testSuite, ExerciceData exercice){
        return getJavaClassName(user, testSuite, exercice)+"Kt";
    }

    public static String getTestName(User user, TestSuite testSuite, ExerciceData exercice, TestData test){
        String fullClassName = getJavaClassName(user, testSuite, exercice);
        if(exercice.getMethodName()==null){
            return fullClassName + "." + test.getName();
        } else {
            String methodName = exercice.getMethodName();
            return fullClassName + "#"+methodName+"." + test.getName();
        }
    }

    protected DynamicTest createDynamicTest(User user, TestSuite testSuite, ExerciceData exercice, TestData test) {
        String testName = getTestName(user, testSuite, exercice, test);



        String classToCall;
        if(user.isKotlin()){
            classToCall=getKotlinClassName(user, testSuite, exercice);
        } else {
            classToCall=getJavaClassName(user, testSuite, exercice);
        }
        return DynamicTest.dynamicTest(testName,
                () -> executeTestCase(classToCall, exercice.getMethodName(), test, exercice.getTestMatchOptionsOrDefault()));
    }

    private void executeTestCase(String fullClassName, String method, TestData test, TestMatchOptions testMatchOptions) {
        try{
            setUpStreams();
            if(method==null)
                reflectionTestExecutor.executeTimedOutTest(TIME_OUT_MILIS, fullClassName, test.getInput(), test.getOutput(), testMatchOptions);
            else
                reflectionTestExecutor.executeTimedOutTest(TIME_OUT_MILIS, fullClassName, method, test.getParamsYaml(), test.getResultYaml(), test.getInput(), test.getOutput());
        } finally {
            restoreStreams();
        }
    }

    public void setTestCasesFolder(String testCasesFolder) {
        this.testCasesFolder = testCasesFolder;
    }

    public void setUserFile(String userFile) {
        this.userFile = userFile;
    }
}
