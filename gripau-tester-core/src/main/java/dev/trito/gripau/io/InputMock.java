package dev.trito.gripau.io;


import dev.trito.gripau.exceptions.GripauInputParsingException;

import java.io.InputStream;

/**
 * Based on org.hyperskill.hstest.dynamic.input.InputMock
 *
 * Instead of using thread group to determine input, it justs overrides de input in each setCurrentInput
 */
public class InputMock extends InputStream {
    public String currentInput;

    public void setCurrentInput(String currentInput) {
        this.currentInput = currentInput+"\n";
    }


    @Override
    public int read(byte[] b, int off, int len) {
        if (len == 0) {
            return 0;
        }

        int c = read();
        if (c == -1) {
            return -1;
        }
        b[off] = (byte) c;

        int i = 1;
        for (; i < len; i++) {
            if (c == '\n') {
                break;
            }
            c = read();
            if (c == -1) {
                break;
            }
            b[off + i] = (byte) c;
        }
        return i;
    }

    @Override
    public int read() {
        if(currentInput.isEmpty()) throw new GripauInputParsingException("Program ran out of input. You tried to read more than expected.");
        char firstChar = currentInput.charAt(0);
        currentInput = currentInput.substring(1);
        return firstChar;
    }
}
