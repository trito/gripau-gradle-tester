package dev.trito.gripau.io;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class OutputHandler {
    private ByteArrayOutputStream outContent;
    private ByteArrayOutputStream errContent;
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;



    public String getOutput(){
        return outContent.toString();
    }

    public Scanner getOutputScanner(){
        return new Scanner(getOutput());
    }

    public void initialize() {
        outContent = new ByteArrayOutputStream();
        errContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    public void restore() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
}
