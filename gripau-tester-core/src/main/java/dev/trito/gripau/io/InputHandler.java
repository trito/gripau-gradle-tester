package dev.trito.gripau.io;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class InputHandler {
    public static InputHandler instance = new InputHandler();
    public static InputHandler get(){ return instance;}

    private final InputMock inputMock = new InputMock();
    private final InputStream stdin = System.in;

    private InputHandler() {
    }

    public void simulateInput(String text){
        inputMock.setCurrentInput(text);
    }

    public void initialize() {
        System.setIn(inputMock);
    }

    public void restore() {
        System.setIn(stdin);
    }
}
