package dev.trito.gripau;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dev.trito.gripau.models.TestMatchOptions;

import java.io.IOException;

import static com.fasterxml.jackson.module.kotlin.ExtensionsKt.jacksonObjectMapper;
import static com.fasterxml.jackson.module.kotlin.ExtensionsKt.registerKotlinModule;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class OutputValidator {

    public static void assertIsValidOutput(TestMatchOptions testMatchOptions, String producedOutput, String desiredOutput){
        if(!isValidOutput(testMatchOptions, producedOutput, desiredOutput)){
            assertEquals(desiredOutput, producedOutput);
        }
    }
    public static boolean isValidOutput(TestMatchOptions testMatchOptions, String producedOutput, String desiredOutput) {
        if(!testMatchOptions.isCaseSensitive()){
            producedOutput = producedOutput.toLowerCase();
            desiredOutput = desiredOutput.toLowerCase();
        }
        if(!testMatchOptions.isSpaceChangeSensitive()){
            producedOutput = producedOutput.replaceAll("\\s+", "");
            desiredOutput = desiredOutput.replaceAll("\\s+", "");
        }
        if(testMatchOptions.isStrictMatchSensitive()){
            return producedOutput.equals(desiredOutput);
        } else {
            return producedOutput.contains(desiredOutput);
        }
    }

    public static void assertIsValidResult(Object result, String desiredResultYaml){
        if(result==null && desiredResultYaml==null)
            return;
        if(desiredResultYaml==null || result==null)
            assertEquals(result, desiredResultYaml);//fail

        ObjectMapper mapper = registerKotlinModule(new ObjectMapper(new YAMLFactory()));

        try {
            Object desiredResult = mapper.readValue(desiredResultYaml, result.getClass());
            assertEquals(result, desiredResult);
        } catch (IOException e) {
            fail(e);
        }
    }


}
